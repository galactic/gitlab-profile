The **GALACTIC** group is organized into 4 subgroups:

* The @galactic/market group is reserved for the market applications. It contains
  python packages describing the classical packages available.
* The @galactic/public group is intended for the general public
  and provides an understanding of the basic elements of lattice algebra
  and its application to basic (historical) **F**ormal **C**oncept **A**nalysis.
* The @galactic/private for private repositories. It contains 2 subgroups:
  - The @galactic/private/shared group is intended for members of the
    [L3i](https://l3i.univ-larochelle.fr/) from the Model and Knowledge team,
    as well as other [L3i](https://l3i.univ-larochelle.fr/) members or established
    researchers from other universities who request access.
  - The @galactic/private/premium group is intended for the commercial part of the
    development, and access is granted through tokens with a limited lifespan reserved
    for entities that have taken out a subscription.
* The @galactic/admin group is reserved for the **GALACTIC** administrators.
  
And 1 special project:

* The galactic/gitlab-profile> project enable this **README**.

